#!/bin/sh

sed -i .bak -E 's:^#(pass in.*services)$:\1:' /etc/pf.conf
service pf reload
service nginx stop