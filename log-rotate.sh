#!/bin/sh

# based on /usr/local/www/gitlab-ce/lib/support/logrotate/gitlab
# which tells that it copytruncate log files

GITLAB_LOGDIR="/usr/local/www/gitlab-ce/log"
GITLABSHELL_LOGDIR="/var/log/gitlab-shell"
NGINX_LOGDIR="/var/log/nginx"

mkdir -p ${GITLAB_LOGDIR}/.rotate
for _log in ${GITLAB_LOGDIR}/*.log; do
    cp -p ${_log} ${GITLAB_LOGDIR}/.rotate/$(basename ${_log}).$(date -j -v -1d +%Y%m%d)
    truncate -s 0 ${_log}
done

mkdir -p ${GITLABSHELL_LOGDIR}/.rotate
cp -p ${GITLABSHELL_LOGDIR}/gitlab-shell.log ${GITLABSHELL_LOGDIR}/.rotate/gitlab-shell.log.$(date -j -v -1d +%Y%m%d)
truncate -s 0 ${GITLABSHELL_LOGDIR}/gitlab-shell.log

mkdir -p ${NGINX_LOGDIR}/.rotate
for _log in ${NGINX_LOGDIR}/*.log; do
    cp -p ${_log} ${NGINX_LOGDIR}/.rotate/$(basename ${_log}).$(date -j -v -1d +%Y%m%d)
    truncate -s 0 ${_log}
done

# based on decision
find ${GITLAB_LOGDIR}/.rotate -type f \! -newerct '1 month ago' -delete
find ${GITLABSHELL_LOGDIR}/.rotate -type f \! -newerct '1 month ago' -delete
find ${NGINX_LOGDIR}/.rotate -type f \! -newerct '1 month ago' -delete
