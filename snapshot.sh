#!/bin/sh

ZFSCMD="/sbin/zfs"

_type=$1
_ds=$2

_snap="$(date +%Y%m%d)_$(date +%H%M%S)"

_prefix=""
_snapcount=0

case ${_type} in
hourly)
    _prefix="${_type}"
    _snapcount=24
    ;;
daily)
    _prefix="${_type}"
    _snapcount=7
    ;;
weekly)
    _prefix="${_type}"
    _snapcount=4
    ;;
esac

if [ -n "${_prefix}" ]; then
    ${ZFSCMD} snapshot -r ${_ds}@${_prefix}_${_snap}
    _tmpdir=$(mktemp -d)
    ${ZFSCMD} list -Hrt snapshot -o name -s creation ${_ds} | grep -E "@${_prefix}.*$" >${_tmpdir}/${_prefix}.list
    if [ $(cat ${_tmpdir}/${_prefix}.list | wc -l | bc) -gt ${_snapcount} ]; then
        tail -n${_snapcount} ${_tmpdir}/${_prefix}.list >${_tmpdir}/${_prefix}.keep
        comm -3 ${_tmpdir}/${_prefix}.list ${_tmpdir}/${_prefix}.keep >${_tmpdir}/${_prefix}.destroy
        while read _s; do
            ${ZFSCMD} destroy -r ${_s}
        done<${_tmpdir}/${_prefix}.destroy
    fi
    rm -rf ${_tmpdir}
fi
