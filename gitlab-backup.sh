#!/bin/sh

GITLAB_BACKDIR="/usr/local/www/gitlab-ce/tmp/backups"

su -l git -c "cd /usr/local/www/gitlab-ce && rake gitlab:backup:create RAILS_ENV=production"

while read _bak; do
    scp -p ${_bak} user@backup.server.com:/backup/gitlab/
done<<DONE
$(find ${GITLAB_BACKDIR} -type f -newerBt '2 hours ago')
DONE

find ${GITLAB_BACKDIR} -type f \! -newerBt '7 days ago' -delete
