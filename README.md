# GitLab Scripts

## Description

These scripts are some scripts I've been using on some FreeBSD instances using GitLab.
Most of them are really simple (KISS) and are using FreeBSD commands.
Websites are using Let's Encrypt (with certbot).